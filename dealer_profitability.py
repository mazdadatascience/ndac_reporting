import pandas as pd
import os
import subprocess
import datetime

from sqlalchemy import create_engine

import mazdap as mz

conn = mz.ObieeConnector()

os.environ["NLS_LANG"] = ".AL32UTF8"

engine = create_engine(
    "oracle+cx_oracle://RPR_STG:rpSgdEv12@x4ebid10.mnao.net:1521/EDWHDEV",
    max_identifier_length=128,
)

today_dt = datetime.date.today()

## dim tables
def get_dim_tables():
    #### DLR/MKT ####
    dlr_master = conn.get_csv("/shared/Operational Strategy/Governance and Process Innovation/WaiPhyo/Dim_Master/All Dealers")
    dlr_master.to_csv("data/dim_dlr_master.csv", index=False)

    #### PARENT/DLR MAPPING ####
    parent_dlr = pd.read_sql("SELECT * FROM WP_PARENT_DEALER_MAPPING", engine)
    parent_dlr.to_csv("data/dim_parent_dlr.csv", index=False)

    #### RE STORES ####
    re_stores = pd.read_sql("SELECT * FROM RPR_STG.TM_RE_STORES", engine)
    re_stores.to_csv("data/dim_re_stores.csv", index=False)

    print("Task: Dimension Tables - Completed.")


## fact tables
def get_fact_tables():
    #### NDAC QUERY ####
    ndac_query = conn.get_csv("/shared/Operational Strategy/Governance and Process Innovation/WaiPhyo/NDAC/ndac_query")
    ndac_query.to_csv("data/fact_ndac.csv", index=False)

    #### RO COUNT/SPEND ####
    ro_count_spend = conn.get_csv("/shared/Operational Strategy/Governance and Process Innovation/WaiPhyo/NDAC/ro_count_spend")
    ro_count_spend.to_csv("data/fact_ro_count_spend.csv", index=False)
    print("Task: Fact Tables - Completed.")

def get_active_dealers():
    result = []
    lastMonth = datetime.date.today().replace(day=1)
    lastMonth_dt = lastMonth.strftime("%m/%d/%Y")
    query = """\
SELECT
	DLR_CD,
	DLR_NM
FROM
	C00_DLY_DEALER_INFO_FW cddif
WHERE
	((STATUS = 'A'
	AND STATUS_DT <= :d)
	OR (STATUS = 'T'
	AND STATUS_DT >= :d)
	OR (STATUS = 'D'
	AND STATUS_DT >= :d)
	OR (STATUS = 'R'
	AND STATUS_DT >= :d))
"""
    for dt in pd.date_range(start="01/01/2019", end=lastMonth_dt, freq="M"):
        df = pd.read_sql(query, con=engine, params={'d': dt})
        df["year_month"] = dt.strftime("%Y-%m-01")
        result.append(df)
    actv_dlrs = pd.concat(result)
    actv_dlrs.to_csv("data/fact_actv_dlrs.csv", index=False)
    print("Task: Active Dealers - Completed.")


#### MAIN ####
get_dim_tables()
get_fact_tables()
get_active_dealers()
#### MAIN ####

#### SYNC ONEDRIVE ####
sync_cmd = "rclone sync -v data sharepoint:wphyo/ndac_dealer_profitability"
with open("logging.txt", "w") as f:
    process = subprocess.Popen(sync_cmd, shell=True, stderr=f, universal_newlines=True)
    while True:
        return_code = process.poll()
        if return_code is not None:
            break
#### SYNC ONEDRIVE ####
